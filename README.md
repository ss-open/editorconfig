# EditorConfig

My personal editorconfig file I use across my projects.

Feel free to use it or to copy it for yourself!

## Documentation

<https://editorconfig.org/>

## Installation

Simply copy the [.editorconfig](./.editorconfig) file to your project root directory.

### Using `wget`

```sh
wget -O .editorconfig https://gitlab.com/ss-open/editorconfig/-/raw/main/.editorconfig
```

### Using `curl`

```sh
curl -o .editorconfig https://gitlab.com/ss-open/editorconfig/-/raw/main/.editorconfig
```
